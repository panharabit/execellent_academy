# EXCELLENT ACADEMY WEBSITE

No Description

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Ruby On Rails (Version >= 5)
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
$ bundle update
$ rake db:create
$ rake db:migrate
$ rake db:reset
```

## Running 

```
$ rails s
```

## Deployment

Add additional notes about how to deploy this on a live system

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

