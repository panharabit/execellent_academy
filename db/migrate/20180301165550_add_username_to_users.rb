class AddUsernameToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :username, :string
    add_column :users, :role, :integer
    add_column :users, :slug, :string
  end
end
