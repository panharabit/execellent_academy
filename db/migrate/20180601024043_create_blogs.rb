class CreateBlogs < ActiveRecord::Migration[5.1]
  def change
    create_table :blogs do |t|
      t.string :title
      t.date :post_date
      t.text :description
      t.integer :type_id
      t.integer :user_id
      t.string :image

      t.timestamps
    end
  end
end
