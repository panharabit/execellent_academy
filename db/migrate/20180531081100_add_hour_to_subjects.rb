class AddHourToSubjects < ActiveRecord::Migration[5.1]
  def change
    add_column :subjects, :hour, :integer
  end
end
