class CreateSections < ActiveRecord::Migration[5.1]
  def change
    create_table :sections do |t|
      t.string :title
      t.text :description
      t.string :image
      t.string :user_id
      t.string :slug
      t.timestamps
    end
  end
end
