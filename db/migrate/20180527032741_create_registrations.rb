class CreateRegistrations < ActiveRecord::Migration[5.1]
  def change
    create_table :registrations do |t|
      t.string :firstname
      t.string :lastname
      t.date :dob
      t.string :email
      t.string :phone
      t.string :address
      t.string :programming
      t.string :image
      t.string :user_id
      t.string :slug
      t.references :course, index: true
      t.boolean :status, default: false
      t.timestamps
    end
  end
end
