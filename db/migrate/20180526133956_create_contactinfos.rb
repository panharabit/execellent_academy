class CreateContactinfos < ActiveRecord::Migration[5.1]
  def change
    create_table :contactinfos do |t|
      t.string :map
      t.string :title
      t.string :phone
      t.string :email
      t.string :address
      t.integer :social_id
      t.integer :user_id
      t.string :slug
      t.timestamps
    end
  end
end
