class CreateCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :courses do |t|
      t.string :course_name
      t.string :sessionrean
      t.time :startfrom
      t.time :endfrom
      t.integer :category_id
      t.decimal :price
      t.text :description
      t.string :image
      t.integer :user_id
      t.integer :instructor_id
      t.string :slug
      t.timestamps
    end
  end
end
