class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :title
      t.date :started_at
      t.date :ended_at
      t.time :start_time
      t.time :end_time
      t.integer :audience_id
      t.string :image
      t.string :slug
      t.text :description
      t.boolean :status

      t.timestamps
    end
  end
end
