class CreateInstructors < ActiveRecord::Migration[5.1]
  def change
    create_table :instructors do |t|
      t.string :firstname
      t.string :lastname
      t.date :dob
      t.string :email
      t.string :phone
      t.text :description
      t.string :title
      t.integer :user_id
      t.string :slug
      t.string :profession
      t.boolean :status, default: true
      t.string :image
      t.timestamps
    end
  end
end
