class AddDurationToCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :duration, :string
  end
end
