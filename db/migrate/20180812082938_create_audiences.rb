class CreateAudiences < ActiveRecord::Migration[5.1]
  def change
    create_table :audiences do |t|
      t.string :username
      t.string :email
      t.string :password_hashL
      t.string :password_salt
      t.string :image
      t.string :phone
      t.string :year

      t.timestamps
    end
  end
end
