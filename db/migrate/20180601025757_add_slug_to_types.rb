class AddSlugToTypes < ActiveRecord::Migration[5.1]
  def change
    add_column :types, :slug, :string
  end
end
