class CreateSubjects < ActiveRecord::Migration[5.1]
  def change
    create_table :subjects do |t|
      t.string :subject_name
      t.string :tags
      t.string :slug
      t.integer :course_id
      t.timestamps
    end
  end
end
