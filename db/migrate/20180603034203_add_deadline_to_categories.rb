class AddDeadlineToCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :deadline, :string
  end
end
