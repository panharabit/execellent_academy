Rails.application.routes.draw do
  # devise_for :users
  namespace :admin do
    root "dashboards#index"
    get "dashboards" => "dashboards#index", :as => :dashboards

    devise_scope :user do
      get "/sign_in" => "devise/sessions#new" # custom path to login/sign_in
      get "/sign_up" => "devise/registrations#new", as: "new_user_registration" # custom path to sign_up/registration
    end

    devise_for :users, controllers: { sessions: 'admin/users/sessions'}, :passwords => 'admin/users/passwords', :skip => [:registrations]

    as :user do
      get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
      put 'users' => 'devise/registrations#update', :as => 'user_registration'
      patch 'users' => 'devise/registrations#update', :as => 'update_user_registration'
    end

    resources :users
    resources :categories
    resources :courses
    resources :subjects
    resources :instructors
    resources :types
    resources :blogs
    resources :registrations
    resources :students, only: [:index, :show]
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "home#index"
  resources :home
  resources :abouts, only: [:index]
  resources :instructors, only: [:index, :show]
  resources :courses, only: [:index, :show] do
    resources :registrations, only: [:show, :create, :new]
  end
  
  resources :contacts
  resources :pages, only: [:index]
  resources :blogs, only: [:index, :show]

  get 'signup' => "audiences#new", :as => :signup
  get 'logout' => "sessions#destroy", :as => :logout
  get 'login' => "sessions#new", :as => :login

  resources :events do
    resources :audiences
  end
end
