class AudiencesController < FrontEndApplicationController
  def new
    @audience = Audience.new
  end

  def create
    @audience = Audience.new(user_params)
    if @audience.save
      session[:user_id] = @user.id
      flash[:notice] = "Thank you for signing up! You are now login in"
      redirect_to "/"
    else
      render :action => "/"
    end
  end

  private
    def user_params
      params.require(:audience).permit(:username, :email, :password, :salt, :encrypted_password)
    end
end
