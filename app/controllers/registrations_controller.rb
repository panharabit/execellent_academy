class RegistrationsController < FrontEndApplicationController
  def new
    @course = Course.find_by_slug(params[:course_id])
    @registration = @course.registrations.new
  end

  def create
    @course = Course.find_by_slug(params[:course_id])
    @registration = @course.registrations.new(registration_params)
    if @registration.save
      redirect_to pages_path
    else
      render :new
    end
  end

  private
    def registration_params
      params.required(:registration).permit(:firstname, :lastname, :email, :phone, :programming)
    end
end
