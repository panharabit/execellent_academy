class BlogsController < FrontEndApplicationController

  def index
    @blogs = Blog.all.where(:status => true)
  end

  def show
    @blog = Blog.find_by_slug(params[:id])
    @blogs = Blog.all.order(id: :desc)
  end

end
