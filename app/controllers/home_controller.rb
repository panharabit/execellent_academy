class HomeController < FrontEndApplicationController
  def index
    @courses = Course.all
    @blogs = Blog.all.where(status: true)
    @available = Registration.all.where(status: true).count
    @contact = Contact.new
  end
end
