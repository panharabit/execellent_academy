class Admin::SubjectsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :set_subject, only: [:show, :edit, :update, :destroy]
  def index
    @search = Subject.ransack(params[:search])
    @subjects = @search.result
  end

  def new
    @subject = Subject.new
  end

  def create
    subject_params[:subject_name].split(',').map do |n|
      Subject.new(subject_name: n).save
    end
    redirect_to new_admin_subject_path, notice: "Subject Successful Created"
  end

  def edit
  end

  def update
    if @subject.update(subject_params)
      redirect_to admin_subject_path(@subject)
    else
      render :edit
    end
  end

  def destroy
    @subject.destroy
    redirect_to admin_subjects_path
  end

  def show
  end

  private
    def set_subject
      @subject = Subject.find(params[:id])
    end

    def subject_params
      params.require(:subject).permit(:subject_name, :hour)
    end
end
