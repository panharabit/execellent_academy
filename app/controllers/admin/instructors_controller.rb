class Admin::InstructorsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  def index
    @instructor = current_admin_user.instructors.build
    @instructors = Instructor.all
  end

  def show
    @instructor = Instructor.find_by_slug(params[:id])
  end

  def new

  end

  def create
    instructor_params[:profession].split(',').map do |n|
      Instructor.new(profession: n).save
    end
    @instructor = current_admin_user.instructors.build(instructor_params)
    if @instructor.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_instructors_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or instructor Duplicated"
        format.html { redirect_to admin_instructors_path }
        format.js { render template: "admin/instructors/instructor_error.js.erb" }
      end
    end
  end

  def edit
    @instructor = Instructor.find_by_slug(params[:id])
  end

  def update
    @instructor = Instructor.find_by_slug(params[:id])
    if @instructor.update(instructor_params)
      redirect_to admin_instructor_path(@instructor), notice: "Successful Updated"
    else
      render :edit
    end
  end

  def destroy
    @instructor = Instructor.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_instructors_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end

    def instructor_params
      params.required(:instructor).permit(:firstname, :lastname, :dob, :email, :phone, :description, :title, :course_id, :profession, :status, :image)
    end
end
