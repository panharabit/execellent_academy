class Admin::DashboardsController < ApplicationController
  before_action :authenticate_admin_user!
  def index
    @registrations = Registration.all.count
    @users = User.all.count
    @students = Registration.all.where(status: true).count
    @blogs = Blog.all.count
    @courses = Course.all.count
    @instructors = Instructor.all.count
    @subjects = Subject.all.count
    @categories = Category.all.count
  end
end
