class Admin::CoursesController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  def index
    if Category.exists?
      @course = current_admin_user.courses.build
      if Instructor.exists?
        @course = current_admin_user.courses.build
        1.times do
          @subject = @course.subjects.build
        end
      else
        flash[:error] = "Please add instructor first"
        redirect_to admin_instructors_path
      end
    else
      flash[:error] = "Please add category first"
      redirect_to admin_categories_path
    end
    @courses = Course.all
  end

  def show
    @course = Course.find_by_slug(params[:id])
  end

  def new

  end

  def create
    @course = current_admin_user.courses.build(course_params)
    if @course.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_courses_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or course Duplicated"
        format.html { redirect_to admin_courses_path }
        format.js { render template: "admin/courses/course_error.js.erb" }
      end
    end
  end

  def edit
    @course = Course.find_by_slug(params[:id])
    if !@course.subjects.exists?
      1.times do
        @subject = @course.subjects.build
      end
    else
      @course = Course.find_by_slug(params[:id])
    end
  end

  def update
    @course = Course.find_by_slug(params[:id])
    if @course.update(course_params)
      redirect_to admin_course_path(@course), notice: "Successful Updated"
    else
      render :edit
    end
  end

  def destroy
    @course = Course.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_courses_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end

    def course_params
      params.required(:course).permit(:course_name, :price, :startfrom, :endfrom, :description,:varn, :sessionrean,:image, :status, :category_id, :instructor_id,
      subjects_attributes: [:id, :_destroy, :subject_name, :hour])
    end
end
