class Admin::StudentsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  def index
    @students = Registration.all.where(status: true)
  end

  def show
    @students = Registration.find_by_slug(params[:id])
  end

  private
    def admin_only?
      unless  current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end
end
