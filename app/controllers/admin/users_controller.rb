class Admin::UsersController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :set_user, only: [:show,:edit,:update,:destroy]
  before_action :set_administrator
  before_action :admin_only, only: [:edit,:update,:destroy,:create]

  def index
    @users = User.all
    @user = User.new
  end

  def show
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(secure_params)
    if @user.save
      respond_to do |format|
        # flash[:success] = "Successful Created"
        flash.now[:notice] =  "#{@user.username} Successful Craeted"
        format.html { redirect_to @user}
        format.js
      end
    else
      respond_to do |format|
        flash.now[:error] = "Email or Username or Password Must Be At Least 6 Charactors"
        format.html { redirect_to new_admin_user_path }
        format.js { render template: 'admin/users/user_error.js.erb' }
      end
    end
  end

  def edit
  end

  def update
    no_requried_password
    if @user.update(user_params)
      redirect_to admin_user_path(@user), notice: "Updated Was Successfully"
    else
      flash[:error] = "Something Went Wrong See The Message Below"
      render :edit
    end
  end

  def destroy
    @user = User.destroy(params[:id])
    @user.destroy
    respond_to do |format|
      flash.now[:error] = "#{@user.username} has been deleted"
      format.html { redirect_to admin_products_url }
      format.js
    end

  end

  private
    def set_user
      @user = User.find_by_slug(params[:id])
    end

    def no_requried_password
      if params[:user][:password].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      end
    end

    def admin_only
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboards_path, :error => "Access denied."
        end
      end
    end

  def secure_params
    params.require(:user).permit(:username,:email,:password,:password_confirmation,
                                  :role,:description,:image,:firstname,:lastname,:position)
  end

  def user_params
    params.require(:user).permit(:username,:email,:password,
                                 :role,:description,:image,:position,:firstname,:lastname)
  end

  def set_administrator
    @admin = User.where({role: "admin"})
  end
end
