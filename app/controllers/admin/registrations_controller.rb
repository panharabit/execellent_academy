class Admin::RegistrationsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  def index
    @registration = current_admin_user.registrations.build
    @registrations = Registration.all
  end

  def show
    @registration = Registration.find_by_slug(params[:id])
  end

  def new

  end

  def create
    @registration = current_admin_user.registrations.build(registration_params)
    if @registration.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_registrations_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or registration Duplicated"
        format.html { redirect_to admin_registrations_path }
        format.js { render template: "admin/registrations/registration_error.js.erb" }
      end
    end
  end

  def edit
    @registration = Registration.find_by_slug(params[:id])
  end

  def update
    @registration = Registration.find_by_slug(params[:id])
    if @registration.update(registration_params)
      redirect_to admin_registration_path(@registration), notice: "Successful Updated"
    else
      render :edit
    end
  end

  def destroy
    @registration = Registration.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_registrations_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end

    def registration_params
      params.required(:registration).permit!
    end
end
