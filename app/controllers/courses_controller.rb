class CoursesController < FrontEndApplicationController
  def index
    @search = Course.where(status: true).ransack(params[:search])
    @courses = @search.result
    @instructors = Instructor.all
  end
  def show
    @courses = Course.all.limit(5).order(id: :desc)
    @course = Course.find_by_slug(params[:id])
  end
end
