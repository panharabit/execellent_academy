class InstructorsController < FrontEndApplicationController
  def index
    @search = Course.ransack(params[:search])
    @courses = @search.result
    @instructors = Instructor.all
  end

  def show
    @instructor = Instructor.find_by_slug(params[:id])
  end
end
