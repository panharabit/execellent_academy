class SessionsController < FrontEndApplicationController
  def new

  end

  def create
    audience = Audience.authenticate(params[:login], params[:password])
    if audience
      session[:audience_id] = audience.id
      flash[:notice] = "Login In Successfully"
      redirect_to_target_or_default("/")
    else
      flash.now[:error] = "Invaild login or password"
      render :action => 'new'
    end
  end

  def destroy
    session[:audience_id] = nil
    flash[:notice] = "You have been logged out."
    redirect_to "/"
  end
end
