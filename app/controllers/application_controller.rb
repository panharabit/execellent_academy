class ApplicationController < ActionController::Base
  layout 'application'
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  protected
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :slug, :role])
      devise_parameter_sanitizer.permit(:account_update, keys: [:username, :slug, :role])
    end

    def after_sign_out_path_for(resource_or_scope)
      admin_user_session_path
    end

    def after_sign_in_path_for(resource_or_scope)
      admin_dashboards_path
    end
end
