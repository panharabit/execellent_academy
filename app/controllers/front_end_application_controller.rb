class FrontEndApplicationController < ActionController::Base
  layout 'front_end_application'
  include Authentication
  protect_from_forgery with: :exception
  before_action :header_nav
  def header_nav
    @search = Course.ransack(params[:search])
    @course = @search.result
  end
end
