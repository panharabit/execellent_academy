class Type < ApplicationRecord
  #Callback
  before_save :update_slug

  #Association
  belongs_to :user
  has_many :blogs

  #Validation
  validates :type_name, presence: true
  validates :type_name, uniqueness: {case_sensitive: false}
  #Method
  def update_slug
    self.slug = type_name.parameterize
  end

  def to_param
    slug
  end
end
