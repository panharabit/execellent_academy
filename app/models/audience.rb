class Audience < ApplicationRecord
  before_save :update_slug

  def update_slug
    self.slug = title.parameterize
  end

  def to_param
    slug
  end
end
