class Blog < ApplicationRecord
  #Callback
  before_save :update_slug

  #Association
  belongs_to :user
  belongs_to :type, required: true

  #Validation

  #Method

  def update_slug
    self.slug = title.parameterize
  end

  def to_param
    slug
  end

  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  validates :title, presence: true
  validates :title, uniqueness: {case_sensitive: false}
end
