class Contact < ApplicationRecord
  #Association

  #Validation
  validates :name, presence: true
  validates :email, presence: true
  #Method
end
