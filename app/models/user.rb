class User < ApplicationRecord
  #CallBack
  before_save :update_slug

  #Association
  has_many :blogs
  has_many :categories
  has_many :instructors
  has_many :courses
  has_many :sections
  has_many :contactinfos
  has_many :registrations
  has_many :types
  has_many :blogs

  #Validation
  validates :username, presence: true
  validates :username , uniqueness: {case_sensitive: false}

  #Method

  def set_default_role
    self.role ||= :user
  end

  def update_slug
    self.slug = username.parameterize
  end

  def to_param
    slug
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable

  enum role: [:user, :vip, :admin]
  after_initialize :set_default_role, :if => :new_record?

  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
