class Event < ApplicationRecord
  before_save :update_slug

  has_many :audiences
  validates :title, uniqueness: { case_sensitive: false }
  def update_slug
    self.slug = title.parameterize
  end

  def to_param
    slug
  end

end
