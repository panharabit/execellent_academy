class Course < ApplicationRecord

  #CallBack
  before_save :update_slug
  #Association
  belongs_to :user, optional: true
  belongs_to :instructor, optional: true
  belongs_to :category
  has_many :subjects, inverse_of: :course
  has_many :registrations

  accepts_nested_attributes_for :subjects, :reject_if => :all_blank, :allow_destroy => true
  #Validation
  validates :course_name, uniqueness: {case_sensitive: false}
  #Method

  def update_slug
    self.slug = course_name.parameterize
  end

  def to_param
    slug
  end

  #Contact Var
  SECTION_SELECT_SESSION = [
    "Morning",
    "Afternoon"
  ]

  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
