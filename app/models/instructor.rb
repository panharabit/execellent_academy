class Instructor < ApplicationRecord
  #callback
  before_save :update_slug
  #Association
  belongs_to :user
  has_many :courses
  #Validation
  validates :firstname, :lastname, presence: true
  validates :email, uniqueness: {case_sensitive: false}
  #Method
  def update_slug
    self.slug = email.parameterize
  end

  def to_param
    slug
  end

  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
