class Subject < ApplicationRecord
  #CallBack

  #Association
  belongs_to :course

  #Validation
  validates :subject_name, presence: true
  #Method

end
