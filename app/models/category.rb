class Category < ApplicationRecord
  #CallBack
  before_save :update_slug

  #Association
  belongs_to :user
  has_many :courses
  #Validation
  validates :category_name, presence: true
  validates :category_name, uniqueness: {case_sensitive: false}

  #Method
  def update_slug
    self.slug = category_name.parameterize
  end

  def to_param
    slug
  end
end
