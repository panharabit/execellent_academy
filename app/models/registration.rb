class Registration < ApplicationRecord
  before_save :update_slug
  #Association
  belongs_to :user, optional: true
  belongs_to :course

  #Validation
  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :email, presence: true
  validates :email, uniqueness: {case_sensitive: false}
  validates :phone, presence: true, :numericality => true
  #Method
  def update_slug
    self.slug = email.parameterize
  end

  def to_param
    slug
  end
end
