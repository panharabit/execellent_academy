module Authentication
  def self.included(controller)
    controller.send :helper_method, :current_audience, :logged_in?, :redirect_to_target_or_default
  end

  def current_audience
    @current_audience ||= Audience.find(session[:audience_id]) if session[:audience_id]
  end

  def logged_in?
    current_audience
  end

  def login_required
    unless logged_in?
      flash[:error] = "You must first log in or sign up before accessing this page"
      store_target_location
      redirect_to login_url
    end
  end

  def redirect_to_target_or_default
    redirect_to(session[:return_to] || default )
    session[:return_to] = nil
  end

  private
    def store_target_location
      session[:return_to] = request.url
    end
end
